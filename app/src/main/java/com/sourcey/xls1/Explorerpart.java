package com.sourcey.xls1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.w3c.dom.Text;

import java.util.HashMap;

public class Explorerpart extends AppCompatActivity
        implements View.OnClickListener{

        TextView tvviewall;

        FrameLayout r1,r2,r3;
    private int Apoints;
    private String SMobile;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent inte = new Intent(Explorerpart.this,LoginActivity.class);
        startActivity(inte);
    }

    @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_explorerpart);

            String point= getIntent().getExtras().getString("Apoints");
            Apoints = Integer.parseInt(point);
            SMobile = getIntent().getExtras().getString("SMobile");

            r1 = (FrameLayout) findViewById(R.id.r1);
            r2 = (FrameLayout) findViewById(R.id.r2);
            r3 = (FrameLayout) findViewById(R.id.r3);

            r1.setOnClickListener(this);
            r2.setOnClickListener(this);
            r3.setOnClickListener(this);


        }

        @Override
        protected void onStop() {
            super.onStop();
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("RemPoints");
                //Apoints = Integer.parseInt(strEditText);*//*

                Intent intent = new Intent();
                intent.putExtra("Apoints", String.valueOf(strEditText));
                setResult(RESULT_OK, intent);
                finish();
            }
        }*/
    }

    @Override
    public void onClick(View v) {
        if(v==r1) {
            //Apoints
            int orpoints = 500;
            if(orpoints>Apoints){
                Toast.makeText(this, "You Need Atleast " + orpoints + " Points", Toast.LENGTH_SHORT).show();
            } else {

                Intent inte = new Intent(Explorerpart.this, ItemDescription.class);
                inte.putExtra("Apoints", 500);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 1);
                //inte.putExtra("points", 350);
                startActivity(inte);
                //startActivityForResult(inte, 1);


                /*Intent inte = new Intent(Explorerpart.this,ItemQrCode.class);
                inte.putExtra("id", 1);
                inte.putExtra("Apoints", 350);
                inte.putExtra("SMobile", SMobile);
                startActivity(inte);*/
            }
        } else if(v==r2) {
            int orpoints = 300;
            if(orpoints>Apoints){
                Toast.makeText(this, "You Need Atleast " + orpoints + " Points", Toast.LENGTH_SHORT).show();
            } else {
                Intent inte = new Intent(Explorerpart.this, ItemDescription.class);
                inte.putExtra("Apoints", 300);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 2);
                //inte.putExtra("points", 350);
                startActivity(inte);

                /*Intent inte = new Intent(Explorerpart.this,ItemQrCode.class);
                inte.putExtra("id", 2);
                inte.putExtra("Apoints", 250);
                inte.putExtra("SMobile", SMobile);
                startActivity(inte);*/
            }
        } else if(v==r3) {
            int orpoints = 200;
            if(orpoints>Apoints){
                Toast.makeText(this, "You Need Atleast " + orpoints + " Points", Toast.LENGTH_SHORT).show();
            } else {

                Intent inte = new Intent(Explorerpart.this, ItemDescription.class);
                inte.putExtra("Apoints", 200);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 3);
                //inte.putExtra("points", 350);
                startActivity(inte);

                /*Intent inte = new Intent(Explorerpart.this,ItemQrCode.class);
                inte.putExtra("id", 3);
                inte.putExtra("Apoints", 100);
                inte.putExtra("SMobile", SMobile);
                startActivity(inte);*/
            }
        }
    }
}