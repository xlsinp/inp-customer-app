package com.sourcey.xls1.rest;

import com.sourcey.xls1.modal.LoginModel;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by bhavik on 30-04-2017.
 */

public interface ApiInterface {
    /*@FormUrlEncoded
    @POST("inp/sureshcvv21@gmail.com/1234")
    public Call<pincodecheck> getPincodeStatus(
            @Field("pincode") String pincode);*/

 /*   @GET("inp/sureshcvv21@gmail.com/1234")
    //public String getCategory();
    Call<ResponseBody> getLoginResponse();
*/

    @GET("/inp/{user}/{pass}/{inp}")
        //public String getCategory();
    Call<LoginModel> getLoginResponse(@Path("user") String user, @Path("pass") String pass,@Path("inp") String inp);


    //fname=Bhavik&mobn=9825516928&email=bhavikbagdai@gmail.com&pwd=1245
    /*@GET("inp/create?fname={fname}&mobn={mobn}&email={email}&pwd={pwd}")
        //public String getCategory();
    Call<ResponseBody> getSignupResponse(@Path("fname") String fname, @Path("mobn") String mobn,@Path("email") String email, @Path("pwd") String pwd);
*/

    @FormUrlEncoded
    @POST("/inp/create")
    public Call<ResponseBody> getSignupResponse(
            @Field("fname") String fname,
            @Field("mobn") String mobn,
            @Field("email") String email,
            @Field("pwd") String pwd,
            @Field("fcmId") String fcmId);


    @FormUrlEncoded
    @POST("/inp/update")
    public Call<ResponseBody> IncreasePoint(
            @Field("mobn") String mobn,
            @Field("str") String str,
            @Field("rewards") String rewards);



    @FormUrlEncoded
    @POST("/inp/update")
    public Call<ResponseBody> DecreasePoint(
            @Field("mobn") String mobn,
            @Field("str") String str,
            @Field("rewards") String rewards);
}