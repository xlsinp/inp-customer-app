package com.sourcey.xls1.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.sourcey.xls1.R;
import com.sourcey.xls1.SignupActivity;

import static android.content.Context.MODE_PRIVATE;

public class DateFragment extends Fragment implements WheelPicker.OnItemSelectedListener, View.OnClickListener {
    View view;
    Button firstButton;
    AppCompatEditText etFname,etLname;

    private WheelPicker wheelLeft;
    private WheelPicker wheelCenter;
    private WheelPicker wheelRight;
    SharedPreferences pref;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_date, container, false);
        pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        firstButton = (Button) view.findViewById(R.id.btnnext);

        wheelLeft = (WheelPicker) view.findViewById(R.id.main_wheel_left);
        wheelLeft.setOnItemSelectedListener(this);
        wheelCenter = (WheelPicker) view.findViewById(R.id.main_wheel_center);
        wheelCenter.setOnItemSelectedListener(this);
        wheelRight = (WheelPicker) view.findViewById(R.id.main_wheel_right);
        wheelRight.setOnItemSelectedListener(this);

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), wheelLeft.getData().get(wheelLeft.getCurrentItemPosition()) + ":" +wheelCenter.getData().get(wheelCenter.getCurrentItemPosition()) + ":" +wheelRight.getData().get(wheelRight.getCurrentItemPosition()) , Toast.LENGTH_SHORT).show();
                ((SignupActivity)getActivity()).loadFragment(new PasswordFragment());
/*                editor.putString("number",etFname.getText().toString());
                editor.commit();*/
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onItemSelected(WheelPicker picker, Object data, int position) {
        String text = "";
        switch (picker.getId()) {
            case R.id.main_wheel_left:
                text = "Left:";
                break;
            case R.id.main_wheel_center:
                text = "Center:";
                break;
            case R.id.main_wheel_right:
                text = "Right:";
                break;
        }
        Toast.makeText(getContext(), wheelLeft.getData().get(wheelLeft.getCurrentItemPosition()) + ":" +wheelCenter.getData().get(wheelCenter.getCurrentItemPosition()) + ":" +wheelRight.getData().get(wheelRight.getCurrentItemPosition()) , Toast.LENGTH_SHORT).show();
    }
}