package com.sourcey.xls1.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sourcey.xls1.LoginActivity;
import com.sourcey.xls1.R;
import com.sourcey.xls1.rest.ApiClient;
import com.sourcey.xls1.rest.ApiInterface;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class PasswordFragment extends Fragment {
    View view;
    Button firstButton;
    AppCompatEditText etFname,etLname;
    SharedPreferences pref;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_password, container, false);
        etFname = (AppCompatEditText)view.findViewById(R.id.etfname);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        FirebaseApp.initializeApp(getActivity().getApplicationContext());

        firstButton = (Button) view.findViewById(R.id.btnnext);
        // perform setOnClickListener on first Button

        pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        //etFname.setText(pref.getString("password",""));

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("password",etFname.getText().toString());
                editor.commit();
                //((SignupActivity)getActivity()).loadFragment(new DateFragment());

                String name = pref.getString("FirstName","") + " " + pref.getString("LastName","");
                String lastname = FirebaseInstanceId.getInstance().getToken();
                String email = pref.getString("email","");
                String mobile = pref.getString("number","");
                String password = etFname.getText().toString();

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                //Toast.makeText(this, "" + lastname, Toast.LENGTH_LONG).show();
                Call<ResponseBody> result = apiService.getSignupResponse(name,mobile,email,password,lastname);
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        try {
                            String resp = response.body().string();
                            if(resp.length()>0){
                                Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                //startActivityForResult(intent, REQUEST_SIGNUP);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });

            }
        });
        return view;
    }
}