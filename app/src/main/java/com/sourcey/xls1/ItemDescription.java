package com.sourcey.xls1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.xls1.rest.ApiClient;
import com.sourcey.xls1.rest.ApiInterface;
import com.squareup.okhttp.ResponseBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.IOException;

public class ItemDescription extends AppCompatActivity {

    ImageView imageView;
    TextView textView,textView1;
    Button button;
    int Apoints,loc,points;
    int RemPoints;
    private String SMobile;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_description);

        /*
        inte.putExtra("Apoints", Apoints);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 1);
                inte.putExtra("points", 350);
         */

        Apoints = getIntent().getExtras().getInt("Apoints");
        SMobile = getIntent().getExtras().getString("SMobile");
        loc = getIntent().getExtras().getInt("loc");
        points = getIntent().getExtras().getInt("points");
        id = getIntent().getExtras().getInt("id");

        init();


    }
    public void init() {
        imageView = (ImageView)findViewById(R.id.imageView);
        textView = (TextView)findViewById(R.id.textView);
        textView1 = (TextView)findViewById(R.id.textView1);
        button = (Button) findViewById(R.id.button);

        if(loc==1) {
            textView1.setText("Free Sweets Sampler box for every 500 points.");
            imageView.setImageResource(R.drawable.offer1);
            textView.setText("This offer deducts 500 points from your account for which you will be given the above Perk.");
        } else if(loc==2) {
            textView1.setText("30% off on Pond's products for 300 points.");
            imageView.setImageResource(R.drawable.offer2);
            textView.setText("This offer deducts 300 points from your account for which you will be given the above perk.");
        } else if(loc==3) {
            textView1.setText("25% off Fresh produce for every 200 points.");
            imageView.setImageResource(R.drawable.offer3);
            textView.setText("This offer deducts 200 points from your account for which you will be given the above Perk.");
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ItemDescription.this, "" + Apoints + "/" + SMobile + "/" + loc + "/" + points , Toast.LENGTH_SHORT).show();

                Intent inte = new Intent(ItemDescription.this,ItemQrCode.class);
                inte.putExtra("id", loc);
                inte.putExtra("Apoints", Apoints);
                inte.putExtra("SMobile", SMobile);
                startActivity(inte);

                /*ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> result = apiService.DecreasePoint(SMobile,"dec", String.valueOf(points));
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        try {
                            String resp = response.body().string();
                            if(resp.length()>0){

                                RemPoints = Apoints-points;
                                *//*Toast.makeText(ItemDescription.this, "Points Reedemed", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent();*//*



                                *//*intent.putExtra("RemPoints", String.valueOf(RemPoints));
                                setResult(RESULT_OK, intent);
                                finish();*//*
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });*/
            }
        });
    }
}