package com.sourcey.xls1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;


public class Reedem extends AppCompatActivity implements View.OnClickListener{
    int Apoints;
    FrameLayout r1,r2,r3,r4;
    private String SMobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reedem);

        init();
    }

    private void init() {
        Apoints= getIntent().getExtras().getInt("Apoints");
        SMobile = getIntent().getExtras().getString("SMobile");

        r1 = (FrameLayout) findViewById(R.id.r1);
        r2 = (FrameLayout)findViewById(R.id.r2);
        r3 = (FrameLayout)findViewById(R.id.r3);
        r4 = (FrameLayout)findViewById(R.id.r4);

        r1.setOnClickListener(this);
        r2.setOnClickListener(this);
        r3.setOnClickListener(this);
        r4.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("Apoints", String.valueOf(Apoints));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v==r1 || v==r4){
            if(Apoints < 350)
                Toast.makeText(this, "You need atleast 350", Toast.LENGTH_SHORT).show();
            else {
                Intent inte = new Intent(Reedem.this, ItemDescription.class);
                inte.putExtra("Apoints", Apoints);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 1);
                inte.putExtra("points", 350);
                startActivityForResult(inte, 1);
            }
             /*   ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> result = apiService.DecreasePoint(SMobile,"dec", "350");
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        try {
                            String resp = response.body().string();
                            if(resp.length()>0){
                                Apoints = Apoints-350;
                                Toast.makeText(Reedem.this, "Points Reedemed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });
            }*/
        } else if(v==r2 ){
            if(Apoints < 250)
                Toast.makeText(this, "You need atleast 250", Toast.LENGTH_SHORT).show();
            else {
                Intent inte = new Intent(Reedem.this, ItemDescription.class);
                inte.putExtra("Apoints", Apoints);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 2);
                inte.putExtra("points", 250);
                startActivityForResult(inte, 1);
            }
            /*if(Apoints < 150)
                Toast.makeText(this, "You need atlest 150", Toast.LENGTH_SHORT).show();
            else{
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> result = apiService.DecreasePoint(SMobile,"dec", "150");
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        try {
                            String resp = response.body().string();
                            if(resp.length()>0){
                                Apoints = Apoints-150;
                                Toast.makeText(Reedem.this, "Points Reedemed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });
            }*/
        } else if(v==r3){
            if(Apoints < 100)
                Toast.makeText(this, "You need atleast 100", Toast.LENGTH_SHORT).show();
            else {
                Intent inte = new Intent(Reedem.this, ItemDescription.class);
                inte.putExtra("Apoints", Apoints);
                inte.putExtra("SMobile", SMobile);
                inte.putExtra("loc", 3);
                inte.putExtra("points", 100);
                startActivityForResult(inte, 1);
            }
            /*if(Apoints < 100)
                Toast.makeText(this, "You need atlest 100", Toast.LENGTH_SHORT).show();
            else{
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ResponseBody> result = apiService.DecreasePoint(SMobile,"dec", "100");
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        try {
                            String resp = response.body().string();
                            if(resp.length()>0){
                                Apoints = Apoints-100;
                                Toast.makeText(Reedem.this, "Points Reedemed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        t.printStackTrace();
                    }
                });
            }*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("RemPoints");
                //Apoints = Integer.parseInt(strEditText);*/

                Intent intent = new Intent();
                intent.putExtra("RemPoints", String.valueOf(strEditText));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}