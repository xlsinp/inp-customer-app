package com.sourcey.xls1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.Random;

/**
 * Created by srikiranvadla on 8/4/2017.
 */

public class ItemQrCode extends AppCompatActivity {
    private static final String TAG = "UserActivity";

    TextView pointsval,nameval,emailval,altid,username,email,mobilenumber,points;
    String Apoints,SMobile;
   // StringBuffer Spointsval,Snameval,Semailval,Saltid,Spointpws,SMobile,SqrCode;
    ImageView qrcode;
    AppCompatButton btnredeem,btnlogout;
    private int WIDTH = 200;
    Bitmap bitmap;
    protected SharedPreferences pref;
    private String id;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        btnlogout.performClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        id = String.valueOf(getIntent().getExtras().getInt("id"));
        Apoints = String.valueOf(getIntent().getExtras().getInt("Apoints"));
        SMobile = getIntent().getExtras().getString("SMobile");

        init();

        String zeropoints = "0"; //This is to send zero points evertime and from merchent we will fetch the points from web service.
        int rand = 0;
        try {
            rand = new Random().nextInt(9999);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qrmar= id /*id*/ + "," + Apoints+ "," + SMobile + "," + rand;

        try {
            int num = Integer.parseInt(id);
            if(num==1) {
                bitmap = TextToImageEncode(qrmar,Color.RED);
            } else if(num==2) {
                bitmap = TextToImageEncode(qrmar,Color.GRAY);
            } else if(num==3) {
                bitmap = TextToImageEncode(qrmar,Color.BLUE);
            } else if(num==4) {
                bitmap = TextToImageEncode(qrmar,Color.GREEN);
            }
            qrcode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        /*nameval.setText(Snameval);
        emailval.setText(Semailval);
        altid.setText(Saltid);
        pointsval.setText(Spointsval);*/
    }

    public Bitmap colorize(Bitmap srcBmp, int dstColor) {

        int width = srcBmp.getWidth();
        int height = srcBmp.getHeight();

        float srcHSV[] = new float[3];
        float dstHSV[] = new float[3];

        Bitmap dstBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                int pixel = srcBmp.getPixel(col, row);
                int alpha = Color.alpha(pixel);
                Color.colorToHSV(pixel, srcHSV);
                Color.colorToHSV(dstColor, dstHSV);

                // If it area to be painted set only value of original image
                dstHSV[2] = srcHSV[2];  // value
                dstBitmap.setPixel(col, row, Color.HSVToColor(alpha, dstHSV));
            }
        }

        return dstBitmap;
    }

    public void refresh(){
        init();

    }
    Bitmap TextToImageEncode(String Value,int color) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    500, 500, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ? color:Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private void init() {
        pointsval = (TextView) findViewById(R.id.pointsval);
        nameval = (TextView) findViewById(R.id.nameval);
        emailval = (TextView) findViewById(R.id.emailval);
        altid = (TextView) findViewById(R.id.altid);
        btnredeem = (AppCompatButton) findViewById(R.id.btn_redem);
        btnlogout = (AppCompatButton) findViewById(R.id.btn_logout);

        btnredeem.setVisibility(View.GONE);
        btnlogout.setText("Cancel");
        username = (TextView) findViewById(R.id.username);
        email = (TextView) findViewById(R.id.email);
        mobilenumber = (TextView) findViewById(R.id.mobilenumber);
        points = (TextView) findViewById(R.id.points);

        nameval.setVisibility(View.GONE);
        emailval.setVisibility(View.GONE);
        altid.setVisibility(View.GONE);
        pointsval.setVisibility(View.GONE);

        username.setVisibility(View.GONE);
        email.setVisibility(View.GONE);
        mobilenumber.setVisibility(View.GONE);
        points.setVisibility(View.GONE);

        qrcode  = (ImageView) findViewById(R.id.qrcode);

        /*qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            int val = Integer.parseInt(pointsval.getText().toString());
            val += 10;
            pointsval.setText("" + val);

            String qrmar=SqrCode + "," + val + "," +Snameval+ "," + SMobile + "," + Semailval+ "," + Saltid;

            try {
                bitmap = TextToImageEncode(qrmar);
                qrcode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
            }

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> result = apiService.IncreasePoint(SMobile,"inr", "10");
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                    try {
                        String resp = response.body().string();
                        if(resp.length()>0){
                            Toast.makeText(UserActivity.this, "Points Updated", Toast.LENGTH_SHORT).show();
                            //handler.sendEmptyMessage(0);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.printStackTrace();
                }
            });
            }
        });*/


        btnredeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent inte = new Intent(UserActivity.this,Reedem.class);
                inte.putExtra("Apoints", pointsval.getText().toString());
                inte.putExtra("SMobile", SMobile);
                startActivityForResult(inte,1);*/
                Intent inte = new Intent(ItemQrCode.this,Explorerpart.class);
                inte.putExtra("Apoints", pointsval.getText().toString());
                inte.putExtra("SMobile", SMobile);
                startActivityForResult(inte, 1);
            }
        });

        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(ItemQrCode.this,LoginActivity.class);
                startActivity(inte);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String strEditText = data.getStringExtra("Apoints");
                pointsval.setText("" + strEditText);
            }
        }
    }



    private class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            String state = extras.getString("extra");
            updateView(state);// update your textView in the main layout
        }
    }

    private void updateView(String state) {
        int Opoint = Integer.parseInt(pointsval.getText().toString()); //Original Points
        int Mpoint = Integer.parseInt(state); //Minus points
        int Rpoint = Opoint + Mpoint;  //Remaining Points
        pointsval.setText(""+ Rpoint);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.my.app.onMessageReceived");
        MyBroadcastReceiver receiver = new MyBroadcastReceiver();
        registerReceiver(receiver, intentFilter);
    }
}